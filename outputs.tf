# -*- coding: utf-8; mode: terraform; -*-

output "starterkit_bastion_public_ip_address" {
  value = "${aws_instance.starterkit_bastion.public_ip}"
}

output "starterkit_instance_private_ip_address" {
  value = "${aws_instance.starterkit_instance.private_ip}"
}

output "starterkit_route53_zone_name_servers" {
  value = "${join(", ", aws_route53_zone.starterkit_route53_zone.name_servers)}"
}
