# -*- coding: utf-8; mode: terraform; -*-

resource "aws_vpc" "starterkit_vpc" {
  cidr_block = "${var.starterkit_cidrs["vpc"]}"

  tags {
    Name = "starterkit-vpc"
  }
}

resource "aws_default_network_acl" "starterkit_default_network_acl" {
  default_network_acl_id = "${aws_vpc.starterkit_vpc.default_network_acl_id}"

  tags {
    Name = "starterkit-default-network-acl"
  }

  ingress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  egress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }
}

resource "aws_default_route_table" "starterkit_default_route_table" {
  default_route_table_id = "${aws_vpc.starterkit_vpc.default_route_table_id}"

  tags {
    Name = "starterkit-default-route-table"
  }
}

resource "aws_subnet" "starterkit_public_subnet" {
  availability_zone = "${var.starterkit_region}a"
  cidr_block        = "${var.starterkit_cidrs["public_subnet"]}"
  vpc_id            = "${aws_vpc.starterkit_vpc.id}"

  tags {
    Name = "starterkit-public-subnet"
  }
}

resource "aws_subnet" "starterkit_private_subnet" {
  availability_zone = "${var.starterkit_region}a"
  cidr_block        = "${var.starterkit_cidrs["private_subnet"]}"
  vpc_id            = "${aws_vpc.starterkit_vpc.id}"

  tags {
    Name = "starterkit-private-subnet"
  }
}

resource "aws_internet_gateway" "starterkit_internet_gateway" {
  vpc_id = "${aws_vpc.starterkit_vpc.id}"

  tags {
    Name = "starterkit-internet-gateway"
  }
}

resource "aws_eip" "starterkit_nat_gateway_eip" {
  vpc = true
}

resource "aws_nat_gateway" "starterkit_nat_gateway" {
  allocation_id = "${aws_eip.starterkit_nat_gateway_eip.id}"
  subnet_id     = "${aws_subnet.starterkit_public_subnet.id}"
}

resource "aws_route_table" "starterkit_public_route_table" {
  vpc_id = "${aws_vpc.starterkit_vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.starterkit_internet_gateway.id}"
  }

  tags {
    Name = "starterkit-public-route-table"
  }
}

resource "aws_route_table_association" "starterkit_public_route_table_association" {
  route_table_id = "${aws_route_table.starterkit_public_route_table.id}"
  subnet_id      = "${aws_subnet.starterkit_public_subnet.id}"
}

resource "aws_route_table" "starterkit_private_route_table" {
  vpc_id = "${aws_vpc.starterkit_vpc.id}"

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.starterkit_nat_gateway.id}"
  }

  tags {
    Name = "starterkit-private-route-table"
  }
}

resource "aws_route_table_association" "starterkit_private_route_table_association" {
  route_table_id = "${aws_route_table.starterkit_private_route_table.id}"
  subnet_id      = "${aws_subnet.starterkit_private_subnet.id}"
}
