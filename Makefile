# -*- coding: utf-8; mode: make; -*-

SHELL = bash

.PHONY: .sshrc all check-variables-defined create destroy format is-defined-% lint request-certificate show-outputs ssh-% ssh-add-keys

all: lint create

is-defined-%:
	@$(if $(value $*),,$(error The environment variable $* is undefined))

check-variables-defined: is-defined-AWS_ACCESS_KEY_ID is-defined-AWS_DEFAULT_REGION is-defined-AWS_SECRET_ACCESS_KEY is-defined-STARTERKIT_DOMAIN

create: check-variables-defined
	@terraform plan -out=terraform.plan				\
	    -var=starterkit_region=\"$(AWS_DEFAULT_REGION)\"		\
	    -var=starterkit_domain=\"$(STARTERKIT_DOMAIN)\"		\
	    .
	@bash --init-file .helpers.sh -i -c 'unless_yes "Apply this plan?"'
	@terraform apply terraform.plan

destroy: check-variables-defined
	@terraform destroy						\
	    -var=starterkit_region=\"$(AWS_DEFAULT_REGION)\"		\
	    -var=starterkit_domain=\"$(STARTERKIT_DOMAIN)\"		\
	    .

format:
	@terraform fmt

lint:
	@terraform validate

request-certificate: check-variables-defined
	@aws acm request-certificate --domain-name "*.$(STARTERKIT_DOMAIN)" --subject-alternative-names "$(STARTERKIT_DOMAIN)"

show-outputs:
	@terraform output

.sshrc: is-defined-STARTERKIT_BASTION_IP_ADDR is-defined-STARTERKIT_INSTANCE_IP_ADDR
	@echo -en							       "\
	  Host *							     \\n\
	    LogLevel quiet						     \\n\
	    StrictHostKeyChecking no					     \\n\
	    UserKnownHostsFile /dev/null				     \\n\
	  Host starterkit-bastion					     \\n\
	    HostName $(STARTERKIT_BASTION_IP_ADDR)			     \\n\
	    User ec2-user						     \\n\
	  Host starterkit-instance					     \\n\
	    HostName $(STARTERKIT_INSTANCE_IP_ADDR)			     \\n\
	    ProxyCommand ssh -F .sshrc starterkit-bastion -W %h:%p	     \\n\
	    User core					      		     \\n\
	" > $@

ssh-add-keys: is-defined-STARTERKIT_BASTION_SSH_KEY is-defined-STARTERKIT_INSTANCE_SSH_KEY
	@$(eval TMP_KEY=$(shell mktemp))
	@chmod 0600 $(TMP_KEY)
	@for SSH_KEY in "$$STARTERKIT_BASTION_SSH_KEY" "$$STARTERKIT_INSTANCE_SSH_KEY";	\
	do									\
	  echo "$$SSH_KEY" > $(TMP_KEY);					\
	  ssh-add $(TMP_KEY) > /dev/null 2>&1;					\
	done
	@rm -f $(TMP_KEY)

ssh-%: .sshrc ssh-add-keys
	@ssh -F .sshrc starterkit-$*
