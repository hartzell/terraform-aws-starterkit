[![build status](https://gitlab.com/tvaughan/terraform-aws-starterkit/badges/master/build.svg)](https://gitlab.com/tvaughan/terraform-aws-starterkit/commits/master)

Terraform AWS Starterkit
===

Quick Start
---

* Purcharse a domain name somewhere.

* Install [Terraform](https://www.terraform.io/downloads.html) and
  [aws-cli](https://github.com/aws/aws-cli#installation).

* Edit [variables.tf](variables.tf) as necessary. Please take special note of
  `starterkit_cidrs`. Default values are provided only for `us-west-2`.

* Go to https://console.aws.amazon.com/iam/home?region=us-west-2#/users and
  create an IAM user with the `AWSCertificateManagerFullAccess`,
  `AmazonEC2FullAccess`, `AmazonRoute53DomainsFullAccess`, and
  `AmazonRoute53FullAccess` permissions.

* Go to
  https://us-west-2.console.aws.amazon.com/ec2/v2/home?region=us-west-2#KeyPairs:sort=keyName
  and create two key pairs, one each for the instance and bastion. These are
  assumed to be called `starterkit-instance` and `starterkit-bastion`.

* Set some environment variables. For example:

        AWS_ACCESS_KEY_ID="IAKA..."
        AWS_SECRET_ACCESS_KEY="PkZ0..."
        AWS_DEFAULT_REGION="us-west-2"
        STARTERKIT_DOMAIN="example.mil"

* `make request-certificate`

    A confirmation email will be sent to the administrative contact associated
    with `$STARTERKIT_DOMAIN`. This certificate request must be confirmed
    before continuing.

* `make`

    Carefully review the plan. Press `Y` to apply the plan or `Enter` to not.

* `make show-outputs`

    Replace the name servers held by the registrar of `$STARTERKIT_DOMAIN`
    with the Route53 zone name servers.

* `make ssh-bastion` or `make ssh-instance`

    This step is optional. Some additional environment variables are
    required. For example:

        STARTERKIT_BASTION_IP_ADDR=$(terraform output starterkit_bastion_public_ip_address)
        STARTERKIT_BASTION_SSH_KEY="
        -----BEGIN RSA PRIVATE KEY-----
        SO/g+bkWs3Q8XbpFKVM94gC4t8VU0+uJf0vVbQvP7zIjR1qWaMNQ8ALEnMLRXJ0uxz+UFp3GBfrA
        BdKSotAvIuuinfG84KyW1bjVKjSGIjLOy/d9uPk2vQbZhFeA85CA3gGYyEwhfHqyzkZ+RDE2dycy
        ...
        -----END RSA PRIVATE KEY-----
        "
        STARTERKIT_INSTANCE_IP_ADDR=$(terraform output starterkit_instance_private_ip_address)
        STARTERKIT_INSTANCE_SSH_KEY="
        -----BEGIN RSA PRIVATE KEY-----
        Pk2vQbZhFeA85CA3gGYyEwhfHqyzkZ+RDE2dycyBdKSotAvIuuinfG84KyW1bjVKjSGIjLOy/d9u
        P7zIjR1qWaMNQ8ALEnMLRXJ0uxz+UFp3GBfrASO/g+bkWs3Q8XbpFKVM94gC4t8VU0+uJf0vVbQv
        ...
        -----END RSA PRIVATE KEY-----
        "

What?
---

An EC2 instance is created on a private subnet behind a load balancer. A
bastion host (another EC2 instance) is created on a public subnet with access
to the private subnet. An elastic IP address is assigned to the bastion host.

AWS Certificate Manager is used to create a wildcard SSL certificate for
`$STARTERKIT_DOMAIN`. Two Route53 DNS entries are created,
`$STARTERKIT_DOMAIN` and `www.$STARTERKIT_DOMAIN`, which point to the public
IP address of the load balancer.

An HTTPS request on port 443 to the load balancer is routed to the EC2
instance over HTTPS on port 8443. An HTTP request on port 80 to the load
balancer is routed to the EC2 instance over HTTP on port 8080. This is
expected to redirect clients back to the load balancer over HTTPS on port 443.

See Also
---

* https://gitlab.com/tvaughan/docker-flask-starterkit
