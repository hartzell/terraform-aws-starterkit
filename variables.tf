# -*- coding: utf-8; mode: terraform; -*-

variable "starterkit_domain" {
  # This must not have a default.
}

variable "starterkit_region" {
  # This must not have a default.
}

variable "starterkit_cidrs" {
  default = {
    vpc            = "10.0.0.0/16"
    private_subnet = "10.0.1.0/24"
    public_subnet  = "10.0.0.0/24"
  }
}

variable "starterkit_instance_count" {
  default = "1"
}

variable "starterkit_instance_type" {
  default = "t2.micro"
}

variable "starterkit_instance_ami" {
  default = {
    # Container Linux (CoreOS)
    us-west-2 = "ami-c177c7a1"
  }
}

variable "starterkit_instance_key_name" {
  default = "starterkit-instance"
}

variable "starterkit_bastion_type" {
  default = "t2.micro"
}

variable "starterkit_bastion_ami" {
  default = {
    # Amazon Linux
    us-west-2 = "ami-7172b611"
  }
}

variable "starterkit_bastion_key_name" {
  default = "starterkit-bastion"
}
